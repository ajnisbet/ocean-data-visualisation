import os
from functools import wraps
from flask import Flask, render_template, Response, request, make_response

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))


def request_wants_tsv():
	best = request.accept_mimetypes.best_match(['text/tab-separated-values', 'text/html'])
	return best == 'text/tab-separated-values' and request.accept_mimetypes[best] > request.accept_mimetypes['text/html']


def add_response_headers(headers={}):
	"""This decorator adds the headers passed in to the response"""
	def decorator(f):
		@wraps(f)
		def decorated_function(*args, **kwargs):
			resp = make_response(f(*args, **kwargs))
			h = resp.headers
			for header, value in headers.items():
				h[header] = value
			return resp
		return decorated_function
	return decorator

@app.route('/')
@add_response_headers({'X-Frame-Options': 'DENY'})
def handler():

	# Return TSV if requested, otherwise default to html
	if request_wants_tsv():
		with open(APP_ROOT + '/static/data/combined.tsv') as f:
			tsv = f.read()
		return Response(tsv, mimetype='text/tab-separated-values')

	return render_template('dash.html')

if __name__ == '__main__':
	app.debug = True
	app.run()