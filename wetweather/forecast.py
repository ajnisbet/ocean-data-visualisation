from __future__ import division
from datetime import datetime

import numpy as np
import statsmodels.api as sm


def do_forecast():
	''' Reads in a weather file, and forecasts some new weather.
	The forecast is a weighted linear model of the last few points.
	forecast.tsv is written, containing the new data.
	combined.tsv is written, containing the past data and the new data, with 
	a new column "forecast", which is 0 for the past data and 1 for the new.
	'''

	# Parameters
	n_forecasts = 10  # number of forecasts to predict
	forecast_strength = 7  # how biased the forecast is to more recent values

	# Load past weather file, converting time strings to timestamps
	epoch = datetime(1970,1,1)
	time_format = '%Y-%m-%d %H:%M:%S'
	convert = lambda x: (datetime.strptime(x, time_format)-epoch).total_seconds()
	weather = np.loadtxt('static/data/metocean.tsv', delimiter='\t', skiprows=1, converters={0: convert})
	
	# Get dataset parameters
	n_rows, n_cols = weather.shape
	dt = weather[-1, 0] - weather[-2, 0]
	n_seconds = n_rows * dt
	n_days = n_seconds/60/60/24

	# Build array to contain new data
	forecast = np.empty((n_forecasts, n_cols))
	forecast[:,0] = (weather[-1, 0]+dt) + np.arange((n_forecasts))*dt

	# Do the prediction
	for col in xrange(1, n_cols):

		# Get last few values
		X = weather[-n_forecasts-1:-1, 0]
		Y = weather[-n_forecasts-1:-1, col]
		X = sm.add_constant(X)

		# Weight the more recent values
		weights = np.arange(Y.size)+1
		weights = weights ** forecast_strength
		weights = weights / weights.sum()
		

		# Fit model
		lm = sm.WLS(Y, X, weights=weights).fit()
		model = lambda x: lm.params[1]*x + lm.params[0]

		# Save prediction
		forecast[:, col] = model(forecast[:, 0])



	# Write prediction to file
	with open('static/data/metocean.tsv') as f:
		header = f.readlines()[0].strip()
	np.savetxt('static/data/forecast.tsv', forecast, delimiter='\t', fmt='%10.2f', header=header, comments='')

	# Quick fix to get the dates working
	with open('static/data/forecast.tsv', 'r') as f:
		lines = f.readlines()
	for i, line in enumerate(lines[1:], 1):
		if not line:
			continue
		values = line.split('\t')
		values[0] = datetime.utcfromtimestamp(int(float(values[0]))).strftime(time_format)
		lines[i] = '\t'.join(values)
	with open('static/data/forecast.tsv', 'w') as f:
		f.writelines(lines)

	# Combine the metocean data with the forcasted data
	with open('static/data/metocean.tsv') as f_metocean, open('static/data/forecast.tsv') as f_forecast:
		metocean_lines = f_metocean.readlines()
		forecast_lines = f_forecast.readlines()

	# Conbine the lines
	header = metocean_lines[0].rstrip('\n') + '\tforecast\n' 
	lines = [header]
	lines += [line.rstrip('\n')+'\t0\n' for line in metocean_lines[1:] if line.strip()]
	lines += [line.rstrip('\n')+'\t1\n' for line in forecast_lines[1:] if line.strip()]

	# Write the result
	with open('static/data/combined.tsv', 'w+') as f:
		f.writelines(lines)




if __name__ == '__main__':
	do_forecast()
