# MetOcean Weather Visualisation

[WetWeather](https://ajnisbet.co.nz/wetweather) is a simple web application for displaying MetOcean data.

## API

The website has a single endpoint, `/`. It will return a `text/html` website. 

## About the data
* Times are in UTC.
* I assumed the data was realtime (which is why the date is 18 Feb), and that the location was Raglan.
* Only the last 3 days of data are displayed, as this is most relevant to the user.
* ~10 hours of data was naively forecast using `wetweather/forecast.py`. It does a weighted linear regression on the last few datapoints. Future data is more useful, but less accurate.


## Libraries and software used
* [ubuntu](http://www.ubuntu.com/server) OS.
* [nginx](http://nginx.org/): web server.
* [uWSGI](http://uwsgi-docs.readthedocs.org/en/latest/WSGIquickstart.html): python wsgi web server.
* [Flask](http://flask.pocoo.org/): python web framework.
* [NumPy](http://www.numpy.org/): python scientific computing package.
* [Statsmodels](http://statsmodels.sourceforge.net/): python statistical modelling package.
* [SASS](http://sass-lang.com/): CSS preprocessor.
* [Bootstrap](http://getbootstrap.com/): CSS framework.
* [d3.js](http://d3js.org/): SVG manipulation.
* [topojson.js](https://github.com/mbostock/topojson): json topology encoding.
* [d3-tip](https://github.com/Caged/d3-tip): tooltips extension for d3.js.

## Limitations
* Times are in UTC, rather than local.
* Current weather values are hard-coded.
* Not responsive.
* Doesn't handle edge cases well enough to have a `PUT` method to add new weather.
* No tooltip/popup thing on the line graph, and the bar ones flash as you move over them.
* Many other things.
